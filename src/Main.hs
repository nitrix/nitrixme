import Web.Scotty (scotty, middleware, get, notFound, html, redirect, text, scottyOpts, Options(..))
import Network.Wai.Middleware.RequestLogger
import Network.Wai.Middleware.Static
import Text.Blaze.Html.Renderer.Text
import Network.Wai.Handler.Warp (setPort, defaultSettings, setHost)

import Content

options :: Options
options = Options
    { verbose  = 0
    , settings = setHost "127.0.0.1"
               $ setPort 9000
               $ defaultSettings
    }

main :: IO ()
main = do
    putStrLn "Waiting for connections..."
    scottyOpts options $ do
        middleware logStdoutDev
        middleware $ staticPolicy (noDots >-> hasPrefix "static/")
        get "/favicon.ico" $ redirect "/static/img/favicon.ico"
        get "/" $ html . renderHtml $ buildPage homePage
        get "/projects" $ html . renderHtml $ buildPage projectsPage
        notFound $ text "Not found"
