module Content where

import Clay (render)
import Styles (allStyles)

import Data.List (intersperse)
import Prelude hiding (div, head)
import Text.Blaze.Html
import Text.Blaze.Html.Renderer.Text
import Text.Blaze.Html5.Attributes hiding (title, style)
import Text.Blaze.Html5 hiding (main, text, map)

buildMenu :: [(AttributeValue, Html)] -> Html
buildMenu = sequence_ . intersperse " · " . map (\(x,y) -> a ! href x $ y)

menuLinks :: [(AttributeValue, Html)]
menuLinks = [ ("/", "Home")
       , ("#", "Blog")
       , ("projects", "Projects")
       , ("#", "Experiments")
       , ("#", "Tools")
       , ("#", "Contact")
       ]

buildPage :: Html -> Html
buildPage page = do
    html $ do
        head $ do
            title "nitrix"
            meta ! httpEquiv "content-type" ! content "text/html; charset=utf-8"
            style ! type_ "text/css" ! media "all" $ lazyText $ render allStyles
        body $ do
            div ! class_ "memyselfandi" $ "nitrix"
            div ! class_ "menu" $ buildMenu menuLinks 
            div ! class_ "content" $ page

homePage :: Html
homePage = do
    div ! class_ "quote" $ do
        p "Welcome on my personal website."
        p "I'm an engineer who loves to write software that is clear, useful and beautiful."
        p $ do
            div "Great software should proceed from clarity of thought and vision,"
            div "fulfill the user's needs, and conjure a sense of creativity and respect."
        p $ do
            div "I am only mortal. The goal is not to live forever;"
            div "but to maybe create something that will."

projectsPage :: Html
projectsPage = do
    div ! class_ "tileset" $ do
        a ! href "#" $ img ! src "https://placehold.it/130x70"
        a ! href "#" $ img ! src "https://placehold.it/130x70"
        a ! href "#" $ img ! src "https://placehold.it/130x70"
        a ! href "#" $ img ! src "https://placehold.it/130x70"
        a ! href "#" $ img ! src "https://placehold.it/130x70"
        a ! href "#" $ img ! src "https://placehold.it/130x70"
        a ! href "#" $ img ! src "https://placehold.it/130x70"
