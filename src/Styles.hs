{-# LANGUAGE OverloadedStrings #-}

module Styles where

import Prelude hiding (repeat)
import Clay

allStyles = do
    body ? do
        margin (px 0) (px 0) (px 0) (px 0)
        padding (px 0) (px 0) (px 0) (px 0)
        backgroundColor "#0d0d0d"
        backgroundRepeat repeat
        backgroundPosition $ positioned (px 0) (px 0)
        backgroundImage $ url "static/img/pattern.png"
        -- background #182025 url(static/img/pattern.png) 0 0 repeat
        -- background
        color "#dddddd"
    fontFace $ do
        fontFamily ["Prism"] []
        fontFaceSrc [FontFaceSrcUrl "static/font/prism.woff" $ Just WOFF
                    ,FontFaceSrcUrl "static/font/prism.otf" $ Just OpenType
                    ,FontFaceSrcUrl "static/font/prism.eot" $ Just EmbeddedOpenType
                    ,FontFaceSrcUrl "static/font/prism.eot?#iefix" $ Just EmbeddedOpenType
                    ,FontFaceSrcUrl "static/font/prismx.ttf" $ Just TrueType
                    ]
    fontFace $ do
        fontFamily ["Covered By Your Grace"] []
        fontFaceSrc [FontFaceSrcUrl "static/font/coveredbyyourgrace.woff2" $ Just WOFF
                    ,FontFaceSrcUrl "static/font/coveredbyyourgrace.otf" $ Just OpenType
                    ,FontFaceSrcUrl "static/font/coveredbyyourgrace.ttf" $ Just TrueType
                    ]
    ".memyselfandi" ? do
        fontSize $ px 84
        height $ px 90
        paddingTop $ px 50
        fontColor "#ffffff"
        textAlign $ alignSide sideCenter
        fontFamily ["Prism"] [sansSerif]
        textShadow (px 0) (px 0) (px 19) (setA 167 $ "#969696")
    ".menu" ? do
        fontFamily ["Covered By Your Grace"] [cursive]
        fontSize $ px 25
        textAlign $ alignSide sideCenter
    ".menu a" ? do
        color "#dddddd"
        textDecoration none
    ".menu a:hover" ? do
        color "#ffffff"
        textShadow (px 0) (px 0) (px 15) (setA 167 $ "#969696")
    ".content" ? do
        paddingTop $ px 57
        fontSize $ px 18
        width $ px 600
        textAlign $ alignSide sideCenter
        left $ pct 50
        margin (px 0) auto auto auto
    ".quote" ? do
        fontFamily ["Covered By Your Grace"] [cursive]
    "a" ? do
        border none (px 0) "#000000"
    ".tileset img" ? do
        width $ px 130
        height $ px 70
        margin (px 5) (px 5) (px 10) (px 10)
        padding (px 1) (px 1) (px 1) (px 1)
        border solid (px 1) "#999999"
    ".tileset img:hover" ? do
        border solid (px 1) ("#FFFFFF")
        boxShadow (px 0) (px 0) (px 15) (setA 167 $ "#969696")
